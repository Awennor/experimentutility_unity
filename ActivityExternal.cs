using UnityEngine;

namespace Pidroh.UnityUtils.Experiment
{
    [CreateAssetMenu(fileName = "ActivityExternal", menuName = "ScriptableObjects /ActivityExternal", order = 1)]
    public class ActivityExternal : ScriptableObject
    {
    }
}