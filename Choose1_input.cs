using UnityEngine;

namespace Pidroh.UnityUtils.Experiment
{

    [CreateAssetMenu(fileName = "Choose1_input", menuName = "ScriptableObjects /Choose1_input", order = 1)]
    public class Choose1_input : InputBase
    {
        [SerializeField]
        int amountOfOptions;

        public int AmountOfOptions
        {
            get
            {
                return amountOfOptions;
            }

            set
            {
                amountOfOptions = value;
            }
        }
    }
}