﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Pidroh.UnityUtils.Experiment
{
    public class ArraysSO_To_Pages : EditorWindow
    {

        [MenuItem("Utilities/Experiment Utilities/Arrays of SO to pages")]
        public static void ShowWindow()
        {
            var window = EditorWindow.GetWindow(typeof(ArraysSO_To_Pages));
            (window as ArraysSO_To_Pages).Init();
        }

        void Init()
        {
            Debug.Log("New serialized Object");

            serializedObject = new SerializedObject(ScriptableObject.CreateInstance<Aux2>());
            //Debug.Log(serializedObject);
            arraysProp = serializedObject.FindProperty("aux");
            Debug.Log(arraysProp.type);
        }

        [Serializable]
        private class Aux 
        {
            public ScriptableObject[] sos;
        }

        [Serializable]
        private class Aux2 : ScriptableObject
        {
            public int random;
            public Aux[] aux;
        }

        //field to add a number so it becomes unique
        string pageName = "";
        //ScriptableObject origin;
        int copies = 1;
        private SerializedProperty arraysProp;
        private SerializedObject serializedObject;


        void OnGUI()
        {

            //assetName = EditorGUILayout.TextField("S. Asset Name:", assetName);

            //target = EditorGUILayout.ObjectField("Label:", target, typeof(GameObject), true) as GameObject;
            //EditorGUILayout.

            if (serializedObject == null) {
                
                Init();
            }

            serializedObject.Update();

            if (arraysProp != null)
                EditorGUILayout.PropertyField(arraysProp, true);
            else
            {
                EditorGUILayout.LabelField("Prop not found");
                arraysProp = serializedObject.FindProperty("aux");
            }

            serializedObject.ApplyModifiedProperties();

            pageName = EditorGUILayout.TextField(pageName);
            Type type = typeof(ScriptableObject);



            if (GUILayout.Button("Create pages"))
            {
                var firstArrayOfSO = arraysProp.GetArrayElementAtIndex(0).FindPropertyRelative("sos");

                List<ScriptableObject> objsAux = new List<ScriptableObject>();
                for (int i = 0; i < firstArrayOfSO.arraySize; i++)
                {
                    objsAux.Clear();
                    objsAux.Add(firstArrayOfSO.GetArrayElementAtIndex(i).objectReferenceValue as ScriptableObject);
                    for (int j = 1; j < arraysProp.arraySize; j++)
                    {
                        var arraysOfSO = arraysProp.GetArrayElementAtIndex(j).FindPropertyRelative("sos");
                        if (arraysOfSO.arraySize > i) {
                            objsAux.Add(arraysOfSO.GetArrayElementAtIndex(i).objectReferenceValue as ScriptableObject);
                        }
                    }


                    Page p = ScriptableObject.CreateInstance<Page>();
                    p.Holders = new Page.ScriptableObjectHolder[1];
                    Page.ScriptableObjectHolder scriptableObjectHolder = new Page.ScriptableObjectHolder();
                    p.Holders[0] = scriptableObjectHolder;
                    scriptableObjectHolder.ScriptableObjects = objsAux.ToArray();

                    AssetDatabase.CreateAsset(p, "Assets/" + pageName +i+".asset");
                }
                





                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                EditorUtility.FocusProjectWindow();

            }

        }



    }
}