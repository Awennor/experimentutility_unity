using System;
using UnityEngine;

namespace Pidroh.UnityUtils.Experiment
{
    [CreateAssetMenu(fileName = "Experiment", menuName = "ScriptableObjects /Experiment", order = 1)]
    public class Experiment : ScriptableObject
    {
        [SerializeField]
        private ExperimentAction[] experimentActions;

        public ExperimentAction[] ExperimentActions
        {
            get
            {
                return experimentActions;
            }

            set
            {
                experimentActions = value;
            }
        }

        [Serializable]
        public class ExperimentAction
        {
            [SerializeField]
            ScriptableObject scriptableObject;
            [SerializeField]
            float minutes = -1;

            public ScriptableObject ScriptableObject
            {
                get
                {
                    return scriptableObject;
                }

                set
                {
                    scriptableObject = value;
                }
            }

            public float Minutes
            {
                get
                {
                    return minutes;
                }

                set
                {
                    minutes = value;
                }
            }
        }
    }
}