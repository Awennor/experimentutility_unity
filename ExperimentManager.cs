﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Pidroh.UnityUtils.Experiment
{
    public class ExperimentManager : MonoBehaviour
    {
        [SerializeField]
        private Experiment experiment;
        public bool autoStartExperiment;
        int objId;
        [SerializeField]
        PageGroupEvent OnShowPageGroup;
        [SerializeField]
        StringEvent OnStartExternalActivity;
        public event Action<string> OnStartExternalActivity_Action;
        public event Action<string> OnExperimentDone_Action;
        private bool waitPageGroupOutput;
        private bool waitActivityOutput;
        [SerializeField]
        StringEvent OnExperimentDone, OnExperimentStart, OnEndActivity;
        private string lastActivityName;
        [SerializeField]
        FloatEvent OnSetSecondsCountdown;

        public bool debugMessages;

        private Action onTimeLimit_UniqueCallback;

        public Action OnTimeLimit_UniqueCallback
        {
            get
            {
                return onTimeLimit_UniqueCallback;
            }

            set
            {
                onTimeLimit_UniqueCallback = value;
            }
        }

        public Experiment Experiment
        {
            get
            {
                return experiment;
            }

            set
            {
                experiment = value;
            }
        }

        // Use this for initialization
        public void Start()
        {
            if (autoStartExperiment)
            {
                if (debugMessages) {
                    Debug.Log("ExperimentManager : autostarting experiment "+Experiment.name);
                }
                StartExperiment();
            }
        }

        public void TimeUp_External()
        {
            Debug.Log("Time up 1");
            if (OnTimeLimit_UniqueCallback != null)
            {
                Debug.Log("Time up 2");
                OnTimeLimit_UniqueCallback();
            }

        }

        public void PageGroupDone_External()
        {
            if (waitPageGroupOutput)
            {
                waitPageGroupOutput = false;
                Advance();
            }

        }

        public void ActivityDone_External()
        {
            Debug.Log("activity done 1");
            if (waitActivityOutput)
            {
                Debug.Log("activity done 2");
                waitActivityOutput = false;
                OnEndActivity.Invoke(lastActivityName);
                Advance();
            }
        }


        private void Advance()
        {
            objId++;
            StartObj();
        }

        public void StartExperiment()
        {
            objId = 0;
            OnExperimentStart.Invoke(Experiment.name);
            StartObj();

        }

        private void StartObj()
        {
            OnSetSecondsCountdown.Invoke(-1);
            var objs = Experiment.ExperimentActions;
            if (objId < objs.Length)
            {

                var obj = objs[objId].ScriptableObject;

                if (obj is Page) {
                    throw new System.ArgumentException("Tried to execute Page as an activity in the Experiment "+Experiment.name+". Use PageGroup instead.", "original");
                }

                if (obj is PageGroup)
                {
                    OnShowPageGroup.Invoke(obj as PageGroup);
                    waitPageGroupOutput = true;
                }
                if (obj is ActivityExternal)
                {
                    waitActivityOutput = true;
                    lastActivityName = obj.name;
                    OnStartExternalActivity.Invoke(lastActivityName);
                    OnStartExternalActivity_Action.Invoke(lastActivityName);
                }

                OnSetSecondsCountdown.Invoke(objs[objId].Minutes * 60);
            }
            else
            {
                OnExperimentDone.Invoke(Experiment.name);
                if(OnExperimentDone != null)
                    OnExperimentDone_Action(Experiment.name);

            }

        }

        // Update is called once per frame
        void Update()
        {

        }

        [Serializable]
        private class PageGroupEvent : UnityEvent<PageGroup> { }
        [Serializable]
        class StringEvent : UnityEvent<string> { }
        [Serializable]
        private class FloatEvent : UnityEvent<float> { }
    }
}