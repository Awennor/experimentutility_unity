﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Pidroh.UnityUtils.Experiment
{
    public class ExperimentStarter : MonoBehaviour
    {
        private ExperimentManager expManager;
        public Experiment experimentSingle;
        public bool start_OnStart;

        // Use this for initialization
        void Start()
        {
            expManager = GetComponentInChildren<ExperimentManager>();
            if (expManager == null) {
                expManager = GameObject.FindObjectOfType<ExperimentManager>();
            }
            if (start_OnStart) {
                StartExperiment();
            }
        }

        public void StartExperiment()
        {
            expManager.Experiment = experimentSingle;
            expManager.StartExperiment();
        }
    }

}
