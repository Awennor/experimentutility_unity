﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Pidroh.UnityUtils.Experiment
{
    public class ExperimentSystemManager : MonoBehaviour
    {
        [SerializeField]
        ExternalActivityEvents[] externalActivityEvents;


        public UnityEvent OnExternalActivityEnd, OnExperimentDone;
        private ExperimentManager expManager;
        

        // Use this for initialization
        void Start()
        {
            expManager = GetComponentInChildren<ExperimentManager>();
            expManager.OnStartExternalActivity_Action += ExpManager_OnStartExternalActivity_Action;
            expManager.OnExperimentDone_Action += ExpManager_OnExperimentDone_Action;

        }

        private void ExpManager_OnExperimentDone_Action(string obj)
        {
            OnExperimentDone.Invoke();
        }

        private void ExpManager_OnStartExternalActivity_Action(string id)
        {
            Debug.Log("STARTING EXTERNAL");
            foreach (var e in externalActivityEvents)
            {
                Debug.Log(e.activityId + "XXX"+id);
                if (e.activityId == id)
                {
                    Debug.Log("STARTING EXTERNAL 2");

                    e.unityEvent.Invoke();
                }
            }
            Debug.Log("STARTING EXTERNAL END");
        }

        public void ExternalActivityEnd_External()
        {
            OnExternalActivityEnd.Invoke();
            expManager.ActivityDone_External();

        }

        [Serializable]
        class ExternalActivityEvents
        {
            public string activityId;
            public UnityEvent unityEvent;

        }

    }

}
