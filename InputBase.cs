using UnityEngine;

namespace Pidroh.UnityUtils.Experiment
{
    public class InputBase : ScriptableObject
    {

        [SerializeField]
        string inputId;
        [SerializeField]
        bool obligatory;

        public bool Obligatory
        {
            get
            {
                return obligatory;
            }

            set
            {
                obligatory = value;
            }
        }

        public string InputId
        {
            get
            {
                return inputId;
            }

            set
            {
                inputId = value;
            }
        }
    }
}