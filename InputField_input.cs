using UnityEngine;
using UnityEngine.UI;
namespace Pidroh.UnityUtils.Experiment
{
    [CreateAssetMenu(fileName = "InputFieldSO", menuName = "ScriptableObjects /InputFieldSO", order = 1)]
    public class InputField_input : InputBase
    {
        [SerializeField]
        string defaultValue = "";
        [SerializeField]
        private InputField.ContentType contentType;
        [SerializeField]
        int characterLimit;

        public string DefaultValue
        {
            get
            {
                return defaultValue;
            }

            set
            {
                defaultValue = value;
            }
        }

        public InputField.ContentType ContentType
        {
            get
            {
                return contentType;
            }

            set
            {
                contentType = value;
            }
        }

        public int CharacterLimit
        {
            get
            {
                return characterLimit;
            }

            set
            {
                characterLimit = value;
            }
        }
    }
}