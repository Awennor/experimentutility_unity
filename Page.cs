using System;
using UnityEngine;
namespace Pidroh.UnityUtils.Experiment
{
    [CreateAssetMenu(fileName = "Page", menuName = "ScriptableObjects /Page", order = 1)]
    public class Page : ScriptableObject
    {

        //public TextAsset[] scriptableObjects;

        [SerializeField]
        private ScriptableObjectHolder[] holders;
        [SerializeField]
        PageLayouts layout;
        [SerializeField]
        TextAnchor anchor = TextAnchor.MiddleCenter;
        [SerializeField]
        bool childControlSizeW, childControlSizeH, childForceExpandW, childForceExpandH;
        [SerializeField]
        RectOffset padding;
        [SerializeField]
        bool childTextContentSizeFitter;
        [SerializeField]
        int childTextFontSize;
        [SerializeField]
        Vector3 childTextSize;
        [SerializeField]
        bool childTextBestFit;
        [SerializeField]
        int childTextBestFit_MaxFont = 40;
        [SerializeField]
        bool exitBlock;

        public ScriptableObjectHolder[] Holders
        {
            get
            {
                return holders;
            }

            set
            {
                holders = value;
            }
        }

        public PageLayouts Layout
        {
            get
            {
                return layout;
            }

            set
            {
                layout = value;
            }
        }

        public TextAnchor Anchor
        {
            get
            {
                return anchor;
            }

            set
            {
                anchor = value;
            }
        }

        public bool ChildControlSizeW
        {
            get
            {
                return childControlSizeW;
            }

            set
            {
                childControlSizeW = value;
            }
        }

        public bool ChildControlSizeH
        {
            get
            {
                return childControlSizeH;
            }

            set
            {
                childControlSizeH = value;
            }
        }

        public bool ChildForceExpandW
        {
            get
            {
                return childForceExpandW;
            }

            set
            {
                childForceExpandW = value;
            }
        }

        public bool ChildForceExpandH
        {
            get
            {
                return childForceExpandH;
            }

            set
            {
                childForceExpandH = value;
            }
        }

        public int ChildTextFontSize
        {
            get
            {
                return childTextFontSize;
            }

            set
            {
                childTextFontSize = value;
            }
        }

        public bool ChildTextContentSizeFitter
        {
            get
            {
                return childTextContentSizeFitter;
            }

            set
            {
                childTextContentSizeFitter = value;
            }
        }

        public RectOffset Padding
        {
            get
            {
                return padding;
            }

            set
            {
                padding = value;
            }
        }

        public Vector2 ChildTextSize
        {
            get
            {
                return childTextSize;
            }

            set
            {
                childTextSize = value;
            }
        }

        public bool ChildTextBestFit
        {
            get
            {
                return childTextBestFit;
            }

            set
            {
                childTextBestFit = value;
            }
        }

        public int ChildTextBestFit_MaxFont
        {
            get
            {
                return childTextBestFit_MaxFont;
            }

            set
            {
                childTextBestFit_MaxFont = value;
            }
        }

        public bool ExitBlock
        {
            get
            {
                return exitBlock;
            }

            set
            {
                exitBlock = value;
            }
        }

        [Serializable]
        public class ScriptableObjectHolder
        {
            [SerializeField]
            private ScriptableObject[] scriptableObjects;

            public ScriptableObject[] ScriptableObjects
            {
                get
                {
                    return scriptableObjects;
                }

                set
                {
                    scriptableObjects = value;
                }
            }
        }
        [Serializable]
        public enum PageLayouts
        {
            Vertical, Horizontal
        }
    }

}