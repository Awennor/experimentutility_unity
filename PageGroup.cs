using UnityEngine;
namespace Pidroh.UnityUtils.Experiment
{
    [CreateAssetMenu(fileName = "PageGroup", menuName = "ScriptableObjects /PageGroup", order = 1)]
    public class PageGroup : ScriptableObject
    {
        [SerializeField]
        Page[] pages;

        public Page[] Pages
        {
            get
            {
                return pages;
            }

            set
            {
                pages = value;
            }
        }
    }
}