﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
namespace Pidroh.UnityUtils.Experiment
{
    public class PageGroupInputGrouper : MonoBehaviour
    {

        public PageVisualization pageVisualization;
        [Serializable]
        private class StringEvent : UnityEvent<string> { }
        [Serializable]
        private class InputAnswerEvent : UnityEvent<string, List<InputAnswer>> { }

        [SerializeField]
        InputAnswerEvent OnInputAnswersReady_AsData;
        [SerializeField]
        StringEvent OnInputAnswersReady_AsString;

        public void PageGroupEnd_External()
        {
            var inputBuffer = pageVisualization.InputIdValuesTempBuffer;

            var values = inputBuffer.Values;
            List<InputAnswer> data = new List<InputAnswer>();
            StringBuilder sB = new StringBuilder();
            foreach (var v in inputBuffer)
            {
                data.Add(new InputAnswer(v.Key, v.Value));

                sB.AppendLine(v.Key);

                sB.AppendLine(v.Value);
                sB.AppendLine();
            }
            OnInputAnswersReady_AsData.Invoke("GROUPNAME", data);
            OnInputAnswersReady_AsString.Invoke(sB.ToString());

        }
    }

    [Serializable]
    public class InputAnswer
    {
        [SerializeField]
        string id, answer;


        public InputAnswer(string key, string value)
        {
            this.id = key;
            this.answer = value;
        }

        public string Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Answer
        {
            get
            {
                return answer;
            }

            set
            {
                answer = value;
            }
        }
    }
}