﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
namespace Pidroh.UnityUtils.Experiment
{
    public class PageGroupManager : MonoBehaviour
    {



        [Serializable]
        private class StringEvent : UnityEvent<string> { }
        [Serializable]
        private class ActionEvent : UnityEvent<Action> { }
        [Serializable]
        private class PageEvent : UnityEvent<Page> { }
        [Serializable]
        private class BoolEvent : UnityEvent<bool> { }
        [SerializeField]
        StringEvent OnPageIndexShow, OnPageGroupStart;
        [SerializeField]
        BoolEvent OnForwardPossible, OnBackwardPossible, OnFinishPossible;
        [SerializeField]
        PageEvent OnShowPage;
        bool pageCanAdvance;
        public bool debug;

        [SerializeField]
        ActionEvent OnRegisterInterruptMethod;


        public UnityEvent OnPageGroupDone, OnPageGroupInterrupt;


        int currentPageIndex = 0;
        [SerializeField]
        private PageGroup pageGroup;

        public bool PageCanAdvance
        {
            get
            {
                return pageCanAdvance;
            }

            set
            {
                pageCanAdvance = value;
                UpdatePageControls();
            }
        }



        public void Interrupt()
        {
            OnPageGroupInterrupt.Invoke();
            FinishButtonPressed_External();
        }

        public void RegisterInterrupt()
        {
            OnRegisterInterruptMethod.Invoke(Interrupt);
        }

        public void PressForward_External()
        {
            ChangeCurrentPageIndex(1);
        }

        private void ChangeCurrentPageIndex(int v)
        {
            currentPageIndex += v;

            UpdatePageIndex();

        }

        public void PressBackwards_External()
        {
            ChangeCurrentPageIndex(-1);
        }

        public void FinishButtonPressed_External()
        {
            OnPageGroupDone.Invoke();
        }

        public void ShowPageGroup(PageGroup pageGroup)
        {

            var pages = pageGroup.Pages;
            this.pageGroup = pageGroup;
            if (debug)
                Debug.Log("Page Group Start");
            OnPageGroupStart.Invoke(pageGroup.name);
            currentPageIndex = 0;
            UpdatePageIndex();

        }

        private void UpdatePageIndex()
        {
            UpdatePageControls();

            OnShowPage.Invoke(pageGroup.Pages[currentPageIndex]);
            string s = (currentPageIndex + 1) + " / " + pageGroup.Pages.Length;
            OnPageIndexShow.Invoke(s);
        }

        private void UpdatePageControls()
        {
            bool canBackwards = true;
            bool canForward = true;
            if (currentPageIndex <= 0)
            {
                canBackwards = false;
                currentPageIndex = 0;
            }
            if (currentPageIndex >= pageGroup.Pages.Length - 1)
            {
                canForward = false;
                currentPageIndex = pageGroup.Pages.Length - 1;
            }

            OnForwardPossible.Invoke(canForward && pageCanAdvance);
            OnBackwardPossible.Invoke(canBackwards);
            OnFinishPossible.Invoke(pageGroup.Pages.Length - 1 == currentPageIndex && pageCanAdvance);
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}