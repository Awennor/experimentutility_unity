﻿using DG.Tweening;
using Pidroh.UnityUtils.Misc;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
namespace Pidroh.UnityUtils.Experiment
{
    public class PageVisualization : MonoBehaviour
    {

        public Page currentPage;
        public Transform pageContainer;
        public Text defaultTextComponent;
        public ToggleGroupAux defaultChoose1Comp;
        public GameObject inputFieldPrefab;
        private Dictionary<string, string> inputIdValuesTempBuffer = new Dictionary<string, string>();
        List<InputBase> inputsOnCurrentPage = new List<InputBase>();
        [SerializeField]
        BoolEvent OnAllObligatorySolved;
        [SerializeField]
        BoolEvent OnPageCanAdvance;
        [SerializeField]
        StringEvent OnPageShow;
        [SerializeField]
        StringStringEvent OnInputReceived;
        public bool debugFlag;

        public Dictionary<string, string> InputIdValuesTempBuffer
        {
            get
            {
                return inputIdValuesTempBuffer;
            }

            set
            {
                inputIdValuesTempBuffer = value;
            }
        }

        [Serializable]
        private class BoolEvent : UnityEvent<bool> { }
        [Serializable]
        private class StringEvent : UnityEvent<string> { }
        [Serializable]
        private class StringStringEvent : UnityEvent<string, string> { }

        [ContextMenu("Show current page")]
        public void ShowCurrentPage()
        {
            OnPageShow.Invoke(currentPage.name);
            pageContainer.DestroyAllChildren();
            pageContainer.transform.SetLocalScaleX(0);

            pageContainer.transform.DOScaleX(1, 0.35f).SetDelay(0.1f);
            inputsOnCurrentPage.Clear();
            pages.Clear();
            pageLayouts.Clear();
            ShowPageRecursive(currentPage, pageContainer);

            //Canvas.ForceUpdateCanvases();
            //foreach (var pl in pageLayouts) {
            //    pl.childControlHeight = true;
            //    pl.childControlWidth = true;
            //}
            //Canvas.ForceUpdateCanvases();
            //Canvas.ForceUpdateCanvases();
            //Canvas.ForceUpdateCanvases();
            //Canvas.ForceUpdateCanvases();
            //Canvas.ForceUpdateCanvases();
            for (int i = 0; i < pages.Count; i++)
            {
                PreparePageLayout_1(pages[i], pageLayouts[i]);
                PreparePageLayout(pageLayouts[i]);
                pageLayouts[i].transform.localScale = Vector3.one;

            }
            
            Canvas.ForceUpdateCanvases();
            

            UpdateInputObligatorySolved();
        }

        private void UpdateInputObligatorySolved()
        {
            var allObligatory = CheckObligatoryInputs();
            Debug.Log(allObligatory);
            OnAllObligatorySolved.Invoke(allObligatory);
            OnPageCanAdvance.Invoke(allObligatory && !currentPage.ExitBlock);
        }

        public void ShowPage(Page p)
        {

            currentPage = p;

            ShowCurrentPage();
        }

        public void ClearInputTempBuffer() {
            inputIdValuesTempBuffer.Clear();
        }

        T CopyComponent<T>(T original, GameObject destination) where T : Component
        {
            System.Type type = original.GetType();
            Component copy = destination.AddComponent(type);
            System.Reflection.FieldInfo[] fields = type.GetFields();
            foreach (System.Reflection.FieldInfo field in fields)
            {
                field.SetValue(copy, field.GetValue(original));
            }
            return copy as T;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        List<Page> pages = new List<Page>();
        List<HorizontalOrVerticalLayoutGroup> pageLayouts = new List<HorizontalOrVerticalLayoutGroup>();

        public void ShowPageRecursive(Page p, Transform pageParentParent)
        {

            if (debugFlag)
                Debug.Log("PageVisualization - Show Page Recursive " + p.name);


            var pageParent = new GameObject("Page Parent", typeof(RectTransform)).transform;
            pageParent.SetParent(pageParentParent, false);


            var l = p.Layout;
            HorizontalOrVerticalLayoutGroup h = null;
            if (l == Page.PageLayouts.Horizontal)
            {
                h = pageParent.gameObject.AddComponent<HorizontalLayoutGroup>();
            }
            if (l == Page.PageLayouts.Vertical)
            {
                h = pageParent.gameObject.AddComponent<VerticalLayoutGroup>();
            }
            pageLayouts.Add(h);
            pages.Add(p);
            //FixLayout(p, pageParent);


            var holders = p.Holders;
            for (int i = 0; i < holders.Length; i++)
            {
                var holder = holders[i];
                var sOs = holder.ScriptableObjects;

                for (int j = 0; j < sOs.Length; j++)
                {
                    var so = sOs[j];
                    if (so is Page)
                    {
                        ShowPageRecursive(so as Page, pageParent);
                    }
                    if (so is GameObjectProvider)
                    {
                        var obj = (so as GameObjectProvider).GameObject;
                        var newObj = Instantiate(obj);
                        newObj.transform.SetParent(pageParent, false);
                    }

                    if (so is SpriteProvider)
                    {
                        SpriteProvider sp = so as SpriteProvider;
                        var sprite = sp.Sprite;

                        var newObj = new GameObject();
                        var image = newObj.AddComponent<Image>();
                        image.preserveAspect = true;
                        image.sprite = sprite;
                        newObj.transform.SetParent(pageParent, false);
                        if(sp.Scale != Vector3.zero)
                            newObj.transform.localScale = sp.Scale;
                        if (sp.Color.a != 0) {
                            image.color = sp.Color;
                        }
                    }

                    if (so is InputBase)
                    {

                        inputsOnCurrentPage.Add(so as InputBase);

                        if (so is Choose1_input)
                        {
                            var choose1 = (so as Choose1_input);
                            var choose1comp = Instantiate(defaultChoose1Comp);
                            choose1comp.transform.SetParent(pageParent, false);
                            choose1comp.VisibleToggles(choose1.AmountOfOptions);

                            string inputId = choose1.InputId;
                            if (InputIdValuesTempBuffer.ContainsKey(inputId))
                            {
                                //Debug.Log("Found input b");
                                choose1comp.SetSelectedToggle(int.Parse(InputIdValuesTempBuffer[inputId]));
                                //inputField.text = ;
                            }
                            else
                            {
                                //Debug.Log("Did not found");
                            }

                            choose1comp.OnActiveToggleChange.AddListener((toggleId) =>
                            {
                                string valueOfInput = toggleId + "";
                                if (toggleId < 0) valueOfInput = "";
                                
                                InputIdValuesTempBuffer[choose1.InputId] = valueOfInput;
                                OnInputReceived.Invoke(inputId, valueOfInput);
                                UpdateInputObligatorySolved();
                            });
                        }

                        if (so is InputField_input)
                        {
                            var nI = so as InputField_input;
                            var inputFieldParent = Instantiate(inputFieldPrefab);
                            var inputField = inputFieldParent.GetComponentInChildren<UnityEngine.UI.InputField>();
                            inputField.contentType = nI.ContentType;
                            inputField.text = nI.DefaultValue;
                            string inputId = nI.InputId;
                            if (InputIdValuesTempBuffer.ContainsKey(inputId))
                            {
                                //Debug.Log("Found input b");
                                inputField.text = InputIdValuesTempBuffer[inputId];
                            }
                            else
                            {
                                //Debug.Log("Did not found");
                            }

                            inputFieldParent.transform.SetParent(pageParent, false);
                            inputField.characterLimit = nI.CharacterLimit;

                            //log data only called on end edit
                            inputField.onEndEdit.AddListener((valueOfInput) =>
                            {
                                OnInputReceived.Invoke(inputId, valueOfInput);
                            });

                            //updates as soon as you get the value
                            inputField.onValueChanged.AddListener((valueOfInput) =>
                            {

                                InputIdValuesTempBuffer[inputId] = valueOfInput;

                                UpdateInputObligatorySolved();
                                //; Debug.Log("Inserted input" + nI.InputId+s);
                            });
                        }


                    }
                    {
                        if (so is StringProviderSO_Abstract)
                        {
                            GameObject go = new GameObject();
                            var t = CopyComponent<Text>(defaultTextComponent, go);
                            string textOrigin = (so as StringProviderSO_Abstract).GetString();
                            textOrigin = textOrigin.Replace("\\n", "\n");
                            t.text = textOrigin;
                            t.color = defaultTextComponent.color;
                            t.font = defaultTextComponent.font;
                            t.fontSize = defaultTextComponent.fontSize;
                            if(p.ChildTextSize.x != 0)
                                t.rectTransform.sizeDelta = p.ChildTextSize;
                            if (p.ChildTextContentSizeFitter) {
                                var cSF = go.AddComponent<ContentSizeFitter>();
                                cSF.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
                            }
                            if (p.ChildTextFontSize > 0) {
                                t.fontSize = p.ChildTextFontSize;
                            }
                            t.resizeTextForBestFit = p.ChildTextBestFit;
                            t.resizeTextMaxSize = p.ChildTextBestFit_MaxFont;
                            go.transform.SetParent(pageParent, false);

                            //go.transform.localScale = Vector3.one;
                            
                        }

                    }
                }

            }
            //FixLayout(p, pageParent);
        }

        private void FixLayout(Page p, Transform pageParent)
        {
            var l = p.Layout;
            if (l == Page.PageLayouts.Horizontal)
            {
                var h = pageParent.gameObject.AddComponent<HorizontalLayoutGroup>();
                //h.childControlWidth = true;
                PreparePageLayout_1(p, h);
                PreparePageLayout(h);
            }
            if (l == Page.PageLayouts.Vertical)
            {
                HorizontalOrVerticalLayoutGroup h = pageParent.gameObject.AddComponent<VerticalLayoutGroup>();
                PreparePageLayout_1(p, h);

                PreparePageLayout(h);
            }
        }

        private static void PreparePageLayout_1(Page p, HorizontalOrVerticalLayoutGroup h)
        {
            
            h.childForceExpandWidth = p.ChildForceExpandW;
            h.childForceExpandHeight = p.ChildForceExpandH;
            h.childControlHeight = p.ChildControlSizeH;
            h.childControlWidth = p.ChildControlSizeW;
            h.childAlignment = p.Anchor;
            h.padding = p.Padding;
        }

        public bool CheckObligatoryInputs()
        {
            for (int i = 0; i < inputsOnCurrentPage.Count; i++)
            {
                var iCP = inputsOnCurrentPage[i];
                if (iCP.Obligatory)
                {
                    if (InputIdValuesTempBuffer.ContainsKey(iCP.InputId))
                    {
                        if (InputIdValuesTempBuffer[iCP.InputId] == "")
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }

                }
            }
            return true;
        }

        public void PreparePageLayout(HorizontalOrVerticalLayoutGroup h)
        {

            h.spacing = 10;
        }
    }
}