﻿using Pidroh.UnityUtils.Experiment;
using Pidroh.UnityUtils.LogDataSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var exp = FindObjectOfType<ExperimentSystemManager>();
        var logData = exp.GetComponentInChildren<LogCapturerManager>();
        var data = logData.LogDatas;

        XMLSerializationHelper<List<LogData>> xmlSerializer = new XMLSerializationHelper<List<LogData>>();
        string xmlS = xmlSerializer.SerializeToStringWithEncode(data);


	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
